<a href="#"><img title="Open Source" src="https://img.shields.io/badge/Open%20Source-%E2%9D%A4-green?style=for-the-badge"></a>
<a href="#"><img title="GPL" src="https://img.shields.io/badge/license-GPL-blue?style=for-the-badge"></a>
# SpammerGmailV1

Envíe un mensaje personalizado multiples veces a una bandeja de entrada de correo electrónico con este pequeño programa de Python.
### (haga esto solo con amigos)

# Uso

Simplemente abra el programa, escriba las credenciales que solicita, el correo objetivo, el mensaje a enviar y la cantidad de mensajes.
# Importante
Es posible que también deba deshabilitar algunas funciones de seguridad en su cuenta para que funcione porque Gmail no permite enviar correos electrónicos de fuentes de terceros de forma predeterminada.
